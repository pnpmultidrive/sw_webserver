# SW_WebServer


## Getting Started

This project uses an integrated Le Potato SBC system, it is possible to use it on other SBCs such as Raspberry pi.

System: [LePotato Libre Compute](https://libre.computer/products/aml-s905x-cc/)
OS: [Raspbian 11 for LePotato](https://hub.libre.computer/t/raspbian-12-bookworm-and-11-bullseye-for-libre-computer-boards/82)

## Installing packages

```
sudo apt update
sudo apt upgrade
sudo apt install lighttpd
git clone https://gitlab.com/pnpmultidrive/sw_webserver.git
cp -R sw_webserver/* ./
```

## lighttpd configuration

open lighttpd configuration file

```
sudo nano /etc/lighttpd/lighttpd.conf
```
replace root to server documents

```
server.document-root        = "/home/kamilo"
```

## serial configuration

```
sudo ldto enable uart-ao-b
sudo minicom -D /dev/ttyAML7
```

## Requirements

-> Python 3.9.2+ (Preinstalled)
-> pip
```
sudo apt install python3-pip
```
-> Websocket Server
```
pip install websocket-server
```
-> OpenCV
```
sudo apt-get install python3-opencv
```
-> pyserial
```
pip install pyserial
```

