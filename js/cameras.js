//Camara mounted in Z-Axis
function Camera1_Decoder(data)
{
    if(data[1] == 'r')//Response
    {
        if(data[2] == 'l')//Camera 1 Launched
        {
            Camera1_StartStreaming();
            document.getElementById("camera1ButtonStatus").value = "Stop";
            document.getElementById("camera1ButtonStatus").className = "btn btn-outline-danger";
        }
        if(data[2] == 's')
        {
            Camera1_StopStreaming();
            document.getElementById("camera1ButtonStatus").value = "Start";
            document.getElementById("camera1ButtonStatus").className = "btn btn-outline-primary";
        }
    }

}

//Down Camera for SMD recognition
function Camera2_Decoder(data)
{
    if(data[1] == 'r')//Response
    {
        if(data[2] == 't')
        {
            document.getElementById("space2img").src = "camera2/photo_" + data.substr(3,data.length) + ".jpg";
        }
    }
}

function Camera1_Set()
{
    selected = document.getElementById('camera1VideoSelector').value
    wssend("1l" + selected);
    //1 -> send comand to camera 1
    //l -> Launch camera
    //n -> Numer of video input (ex. 1->Video1)
}

function Camera2_Set()
{
    selected = document.getElementById('camera2VideoSelector').value
    wssend("2s" + selected);
    //2 -> send comand to camera 2
    //s -> set camera
    //n -> Numer of video input (ex. 1->Video1)
}

function Camera2_TakePhoto()
{
    wssend("2t");
    //2 -> send comand to camera 2
    //t -> take photo
}

function Camera1_StartStreaming()
{
    var c = document.getElementById("Camera1Pointer");
    var ctx = c.getContext("2d");
    const url = new String(window.location);
    const cam_url = new String(url.split('http://')[1]);
    const cam_str = new String('http://' + cam_url.split('/')[0] + ':8081/?action=stream');
    document.getElementById('camStreamer').src = cam_str;
    ctx.lineWidth = 2;
    ctx.strokeStyle = "#f00";
    //vertical line
    ctx.beginPath();
    ctx.moveTo(240, 0);
    ctx.lineTo(240, 360);
    ctx.stroke();
    //Horizontal line
    ctx.strokeStyle = "#00f";
    ctx.beginPath();
    ctx.moveTo(0, 180);
    ctx.lineTo(480, 180);
    ctx.stroke();
}

function Camera1_StopStreaming()
{
    var c = document.getElementById("Camera1Pointer");
    const ctx = c.getContext('2d');
    ctx.clearRect(0, 0, c.width, c.height);
    document.getElementById('camStreamer').src = "";
}