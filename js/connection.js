var connected = false;
var header = 0;

function wsConnect()
{
    
    if(connected == false)
    {
        const url = new String(window.location);
        const ws_url = new String(url.split("http://")[1]);
        const ws_str = new String("ws://" + ws_url.split("/")[0] + ":9001");

        //console.log("conn: " + ws_str);
        ws = new WebSocket(ws_str);
        ws.onerror = onError;
        ws.onopen = onOpen;
        ws.onmessage = onMessage;
        ws.onclose = onClose;
    }
    else
    {
        ws.close();
        document.getElementById("connectionStatus").innerHTML = "Status: Disconnected";
        document.getElementById("connectionStatus").className = "connectionDisconnected";
        document.getElementById("connectionButtonStatus").value = "Connect";
        document.getElementById("connectionButtonStatus").className = "btn btn-outline-primary";
        connected = false;
    }
}

function onOpen ()
{
    connected = true;
    document.getElementById("connectionStatus").innerHTML = "Status: Connected";
    document.getElementById("connectionStatus").className = "connectionConnected";
    document.getElementById("connectionButtonStatus").value = "Disconnect";
    document.getElementById("connectionButtonStatus").className = "btn btn-outline-danger";
    consolePrint("Connection opened\n");
};

function onMessage (evt)
{
    consolePrint("Rev: " + evt.data + "\n");
    if(evt.data[0] == '0')
    {
        //gcode event
        GCODE_Decode(evt.data);
    }
    if(evt.data[0] == '1')
    {
        //camera 1 event
        Camera1_Decoder(evt.data)
    }
    if(evt.data[0] == '2')
    {
        //camera 2 event
        Camera2_Decoder(evt.data)
    }
};

function onClose (event)
{
    document.getElementById("connectionStatus").innerHTML = "Status: Disconnected";
    document.getElementById("connectionStatus").className = "connectionDisconnected";
    document.getElementById("connectionButtonStatus").value = "Connect";
    document.getElementById("connectionButtonStatus").className = "btn btn-outline-primary";
    connected = false;
    consolePrint("Connection closed\n");
};

function onError()
{
    document.getElementById("connectionStatus").innerHTML = "Status: Error";
    document.getElementById("connectionStatus").className = "connectionError";
    consolePrint("Websocker Error\n");
}

function wssend(msg)
{
    if(connected == true)
    {
        if(msg != "")
        {
            ws.send(msg+"\n");
            consolePrint("Send: " + msg + "\n");
        }
        else
        {
            consolePrint("Console: Empty\n");
        }
    }
    else
    {
        consolePrint("Not Connected\n");
    }
}