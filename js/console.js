function consolePrint(text)
{
    var log = document.getElementById("console");
    log.value += (text);
    log.scrollTop = log.scrollHeight;
}

function consoleClean()
{
    document.getElementById("console").value = "";
}
