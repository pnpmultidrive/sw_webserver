var controlPressed = false;
var controlLastKey = "";

document.addEventListener('keydown', controlEventKeydown, false);
document.addEventListener('keyup', controlEventKeyup, false);

function controlSendCMD(dir)
{
    var feed = document.getElementById("controlFeed").value;
    var leng = document.getElementById("controlLeng").value;
    var str = "";
    switch(dir)
    {
        case 0://X+
            str = "G1X"+leng+"F"+feed+";";
            break;
        case 1://X-
            str = "G1X-"+leng+"F"+feed+";";
            break;
        case 2://Y+
            str = "G1Y"+leng+"F"+feed+";";
            break;
        case 3://Y-
            str = "G1Y-"+leng+"F"+feed+";";
            break;
        case 4://Z-
            str = "G1Z-"+leng+"F"+feed+";";
            break;
        case 5://Z+
            str = "G1Z"+leng+"F"+feed+";";
            break;
        case 6://Stop CMD
            str = "M0";
            break;
        default:
            str = document.getElementById("connectionInput").value
            break;
    }
    wssend(str);
}

function controlEventKeydown(event)
{
    var keyValue = event.key;
    var codeValue = event.code;

    if(controlPressed) return;

    if(keyValue == "ArrowLeft")
    {
        wssend("G1X-400.0F15;");
        controlPressed = true;
        controlLastKey = keyValue;
    }
    if(keyValue == "ArrowRight")
    {
        wssend("G1X400.0F15;");
        controlPressed = true;
        controlLastKey = keyValue;
    }
    if(keyValue == "ArrowUp")
    {
        wssend("G1Y-400.0F15;");
        controlPressed = true;
        controlLastKey = keyValue;
    }
    if(keyValue == "ArrowDown")
    {
        wssend("G1Y400.0F15;");
        controlPressed = true;
        controlLastKey = keyValue;
    }
}

function controlEventKeyup(event)
{
    var keyValue = event.key;
    var codeValue = event.code;

    if(keyValue == controlLastKey)
    {
        wssend("M0");
        controlPressed = false;
    }
}
  
function controlEventFocus()
{
    document.removeEventListener('keydown', controlEventKeydown, false);
    document.removeEventListener('keyup', controlEventKeyup, false);
}

function controlEventBlur()
{
    document.addEventListener('keydown', controlEventKeydown, false);
    document.addEventListener('keyup', controlEventKeyup, false);
}