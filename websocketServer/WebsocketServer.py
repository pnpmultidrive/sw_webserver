#!/usr/bin/env python

from websocket_server import WebsocketServer
import serial
import threading
import time
import os

from camera1 import *
from camera2 import *
from gcode import *

lastClient = None
def new_client(client, server):
	global lastClient
	lastClient = client
	print("Connected: %d" % client['id'])


def client_left(client, server):
	print("Disconnected: %d" % client['id'])

def message_received(client, server, message):
	#print("Cliente: (%d) msg: %s" % (client['id'], message))
	if message[0] == '1':
		res = Camera1_Decode(message)
		if res != 0:
			server.send_message(client, res)
	elif message[0] == '2':
		res = Camera2_Decode(message)
		if res != 0:
			server.send_message(client, res)
	else:
		ser.write(message.encode())

def serialTask(ser):
	line = ""
	while True:
		if ser.in_waiting > 0:
			char = ser.read()
			if char.decode("utf-8") != "\n":
				line = line + char.decode("utf-8")
			else:
				#print("SERIAL: " + line)
				server.send_message(lastClient, line)
				line = ""
		else:
			time.sleep(0.05)
os.system("sudo ldto enable uart-ao-b")
time.sleep(1)
ser = serial.Serial('/dev/ttyAML6', 115200, timeout=0)
serTask = threading.Thread(target = serialTask, args=[ser])
serTask.start()

server = WebsocketServer(host='0.0.0.0', port=9001)
server.set_fn_new_client(new_client)
server.set_fn_client_left(client_left)
server.set_fn_message_received(message_received)
print("Server running")
server.run_forever()
#G1Y20.000F1000;