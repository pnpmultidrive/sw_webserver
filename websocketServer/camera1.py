#!/usr/bin/env python

import os
import time
from subprocess import check_output

camera1State = 0 #False -> Stopping, True -> Running

def Camera1_Decode(msg):
	print("Camera1: " + msg)
	if msg[1] == 'l': #Configure camera and Launch streaming
		res = Camera1_Launch(msg)
		if res == 0:
			print("Camera1 Launched")
			return "1rl" #1-> Camera 1, r -> response, l-> launch command
		if res == 1:
			print("Camera1 Stopped")
			return "1rs"
	return 0

def Camera1_Launch(msg):
	global camera1State
	if camera1State == 0:
		res = os.system('/home/kamilo/streamer/mjpg_streamer -i "/home/kamilo/streamer/libraries/input_uvc.so -d /dev/video' + msg[2] + ' -r 640x480 -f 25" -o "/home/kamilo/streamer/libraries/output_http.so -p 8081 -w /home/kamilo/streamer/www" > /dev/null &')
		camera1State = 1
		time.sleep(0.2)
		if res == 0:
			return 0
	else:
		camera1State = 0
		pid = check_output(["pidof","mjpg_streamer"])
		killCommand = "kill " + str(pid)[2:-3]
		print(killCommand)
		res = os.system(killCommand)
		if res == 0:
			return 1
	return 2
	#print("Camera1 Response: " + str(res))
	#pid = check_output(["pidof","mjpg_streamer"])
	#camera1PID = str(pid)[2:-3]
	#print(camera1PID)

