#!/usr/bin/env python
import cv2
import os

camera2PhotoCounter = 0
camera2IDCamera = 3

def Camera2_Decode(msg):
	global camera2IDCamera
	print("Camera2: " + msg)
	if msg[1] == 's': #Set Camera 2 configuration
		camera2IDCamera = int(msg[2])
		return "2rs"
	if msg[1] == 't':
		return Camera2_TakePhoto()
	return 0

def Camera2_TakePhoto():
	global camera2PhotoCounter
	camera2PhotoCounter = camera2PhotoCounter + 1
	image_path = '/home/kamilo/camera2/photo_' + str(camera2PhotoCounter) + '.jpg'
	cap = cv2.VideoCapture(camera2IDCamera)
	ret,frame = cap.read()
	if not ret:
		print("Failed capturing frame")
		return 0
	#img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	cv2.imwrite(image_path, frame)	
	return "2rt" + str(camera2PhotoCounter)